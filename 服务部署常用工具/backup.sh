#!/bin/sh

defaultPath=/usr/tmp/backup
currentDate=$(date +%Y%m%d)

if [ ! -n "$1" ]; then
  echo '文件分类为空...'
  exit 0
fi

backupPath="$defaultPath/$1/$currentDate"


if [ ! -d "$backupPath" ]; then
  echo '文件夹不存在，开始创建...'
  mkdir -p "$backupPath"
fi

if [ ! -n "$2" ]; then
  echo '请选择需要迁移的文件！'
  exit 0
fi

if [ ! -f "$2" ]; then
  echo "[$2]文件不存在..."
  exit 0
fi

echo '复制文件'$2到$backupPath文件夹

if [ ! -f "$backupPath/$2" ]; then
  cp $2 $backupPath
  echo "查看：ls $backupPath/$2"
  exit 0
fi

mv "$backupPath/$2" "$backupPath/$2.$(date +%s)"
cp $2 "$backupPath/$2"

echo "查看：ls $backupPath/$2"
